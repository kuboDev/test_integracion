var express = require('express');
var router = express.Router();
var mongoose = require("mongoose");

var Producto = require("../model/producto");

/* GET home page. */
router.get('/', function (req, res, next) {

  Producto.find({
    },
    function (err, respuesta) {
      if (err) {
        res.send(err);
      } else {
        console.log('Respuesta Servicio', respuesta);
        res.send(respuesta);
      }
    }
  );

});

router.get('/producto/:id', function (req, res, next) {

  Producto.findById({_id: req.params.id}, req.body, {new: true}).then(function(producto){
    res.send(producto);
  });

});

router.get('/productoid/:id', function (req, res, next) {

  Producto.findOne({id: req.params.id}, req.body, {new: true}).then(function(producto){
    res.send(producto);
  });

});

/* UPDATE PRODUCTO */
router.put('/productoid/:id', function(req, res, next) {


  Producto.findOneAndUpdate({id: req.params.id}, req.body, {new: true}).then(function(producto){
    res.send(producto);
  });


 });
  
/* UPDATE PRODUCTO */
router.put('/:id', function(req, res, next) {


  Producto.findByIdAndUpdate({_id: req.params.id}, req.body, {new: true}).then(function(producto){
    res.send(producto);
  });


 });

 /* UPDATE POR GET PRODUCTO */
router.get('/update/', function(req, res, next) {

  console.log(req.body);
  Producto.findOne({ _id: req.body.id }).exec((err, result) => {
    if (result) {
      result.stock = req.body.stock;
      result.save((err) => {
        if (err) {
          res.send("ERROR");
        }
        res.send("STOCK ACTUALIZADO");
      });
    }
  });

 });

router.post('/', function (req, res) {

  console.log('Datos de Entrada', req.body);

  var producto = new Producto({
    id: req.body.id,
    nombre: req.body.nombre,
    marca: req.body.marca,
    stock: req.body.stock,
    precio: req.body.precio
  });

  console.log('Datos de producto', producto);


  producto.save(function (err, resp) {
    if (err) res.send(err);
    else {
      res.send(resp);
    }
  });

});

module.exports = router;
