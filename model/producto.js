var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var productoSchema = new Schema({
    id: Number,
    nombre: String,
    marca: String,
    stock: Number,
    precio: Number
});

module.exports = mongoose.model('Producto', productoSchema, 'producto');